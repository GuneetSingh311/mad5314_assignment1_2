//
//  InterfaceController.swift
//  MAD5314_Assignment1_2 WatchKit Extension
//
//  Created by Guneet on 2019-03-16.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController,WCSessionDelegate {
   
    // MARK: Outlets
    @IBOutlet weak var Departure: WKInterfaceLabel!
    @IBOutlet weak var Destination: WKInterfaceLabel!
    @IBOutlet weak var Date: WKInterfaceLabel!
    @IBOutlet weak var Number: WKInterfaceLabel!
    @IBOutlet weak var Price: WKInterfaceLabel!
    
    // MARK: Variables
    
    var price:String?
    var number:Int?
    var date:String?
    var departure:String?
    var destination:String?
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        // MARK: Check if the session is supported
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
            print("WATCH  - Activated a session!")
        }
        
        
        
    }
    
    // MARK: WKSession function used by our app
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        if (message["flightPrice"] != nil)
        {
            //MARK: getting flightPrice
            let price = message["flightPrice"] as! String
            print(price)
            self.price = price
            self.Price.setText("$: \(price)")
        }
        
        if(message["flightDate"] != nil)
        {
            //MARK: getting flightDate
            let date = message["flightDate"] as! String
            print(date)
            self.date = date
            self.Date.setText(date)
        }
        
        if(message["flightNumber"] != nil)
        {
            //MARK: getting flightNumber
            let number = message["flightNumber"] as! Int
            print(number)
            self.number = number
            self.Number.setText(String(describing: number))

        }
        if(message["flightDeparture"] != nil)
        {
            //MARK: getting flightDeparture
            let departure = message["flightDeparture"] as! String
            print(departure)
            self.departure = departure
            self.Departure.setText(departure)
        }
        if(message["flightDestination"] != nil)
        {
            //MARK: getting flightDestination
            let destination = message["flightDestination"] as! String
            self.destination = destination
            print(destination)
            self.Destination.setText(destination)
        }
        
        
        
    
    }
    
    
    // MARK: Booking Action
    
    
    @IBAction func bookingAction() {
        
        if (WCSession.default.isReachable)
        
        {

            let flightNumber = ["flightNumber": self.number!]
            //MARK: send flightNumber to watch
            WCSession.default.sendMessage(flightNumber, replyHandler: nil)
            let flightPrice = ["flightPrice":self.price!]
            //MARK: send flightPrice to watch
            WCSession.default.sendMessage(flightPrice, replyHandler:nil)
            let destination = ["flightDestination": self.departure!]
            //MARK: send destination to watch
            WCSession.default.sendMessage(destination, replyHandler: nil)
            let departure  = ["flightDeparture":self.destination!]
            //MARK: send departure to watch
            WCSession.default.sendMessage(departure, replyHandler:nil)
            let date = ["flightDate": self.date!]
            // MARK: send date to watch
            WCSession.default.sendMessage(date, replyHandler: nil)
            
        
        
        }
        
        
    }
    
    
    
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
