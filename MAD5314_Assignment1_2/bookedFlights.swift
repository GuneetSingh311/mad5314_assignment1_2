//
//  bookedFlights.swift
//  MAD5314_Assignment1_2
//
//  Created by Guneet on 2019-03-17.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit

class bookedFlights: UIViewController,UITableViewDelegate,UITableViewDataSource {

    

    @IBOutlet weak var bookedFlightsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = values[indexPath.row]
        return cell
        
        
    }
}
