//
//  ViewController.swift
//  MAD5314_Assignment1_2
//
//  Created by Guneet on 2019-03-16.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity

var values  = [String]()
class ViewController: UIViewController,WCSessionDelegate {
   
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    // MARK: outlets
    @IBOutlet weak var searchFlights: UITextField!
    @IBOutlet weak var searchFlightsButton: UIButton!
    @IBOutlet weak var searchToFlights: UITextField!
    
    // MARK: variables
    
    var fromPlace = ""
    var toPlace  = ""
    var destination  = ""
    var departure  = ""
    var flightPrice  = ""
    var flightNumber  = 0
    var flightName  = ""
    var flightDate  = ""
    var controller = 1
    
    // MARK: values from watch stored in these variables
    var getprice:String?
    var getNumber:Int?
    var getDate:String?
    var getDeparture:String?
    var getDestination:String?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // MARK: Making button round
        self.searchFlightsButton.layer.cornerRadius = self.searchFlightsButton.frame.height/2
        
        
        // MARK check if the session is supported
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
}

    @IBAction func searchButtonAction(_ sender: Any) {
        // MARK: assign textfields values to variables
        self.fromPlace = self.searchFlights.text!
        self.toPlace = self.searchToFlights.text!

        // MARK: using alamofire to send request to api
        Alamofire.request("https://5c89560e41fb3f001434bda5.mockapi.io/flights").responseJSON { response in
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            
            }
            
            // OUTPUT the json response to the terminal
           print(apiData)
            
            
            // GET something out of the JSON response
            let jsonResponse = JSON(apiData)
            print(jsonResponse["items"][0]["Destination"])
            for i in 0...60 {
             // checking wether the values exist in api for particular departure and
            // destination values.
            if(jsonResponse["items"][i]["Departure"].string == self.fromPlace)
            {
            if(jsonResponse["items"][i]["Destination"].string == self.toPlace)
           
            {
                
                print(jsonResponse["items"][i])
                print(jsonResponse["items"][i]["Destination"])
                print(jsonResponse["items"][i]["Departure"])
                self.flightPrice = jsonResponse["items"][i]["flightPrice"].string!
                self.flightNumber = jsonResponse["items"][i]["flightNumber"].int!
                self.departure = jsonResponse["items"][i]["Departure"].string!
                self.destination = jsonResponse["items"][i]["Destination"].string!
                self.flightDate = jsonResponse["items"][i]["flightDate"].string!
                self.performSegue(withIdentifier: "toFlights", sender: nil)
                }}}}
}
    
    
    
  
    
     func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
   
        if (message["flightPrice"] != nil)
        {
            //MARK: getting flightPrice
            let price = message["flightPrice"] as! String
            print(price)
            self.getprice = price
        }
        
        if(message["flightDate"] != nil)
        {
            //MARK: getting flightDate
            let date = message["flightDate"] as! String
            print(date)
            self.getDate = date
            
        }
        
        if(message["flightNumber"] != nil)
        {
            //MARK: getting flightNumber
            let number = message["flightNumber"] as! Int
            print(number)
            self.getNumber = number
            
        }
        if(message["flightDeparture"] != nil)
        {
            //MARK: getting flightDeparture
            let departure = message["flightDeparture"] as! String
            print(departure)
            self.getDeparture = departure
       
        }
        if(message["flightDestination"] != nil)
        {
            //MARK: getting flightDestination
            let destination = message["flightDestination"] as! String
            self.getDestination = destination
           
}
       self.controller = 2
}
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(self.controller==1) {
        let vc = segue.destination as! flightDetailController
        vc.flightPrice = self.flightPrice
        vc.fromPlace = self.departure
        vc.toPlace = self.destination
        vc.flightDate = self.flightDate
        vc.flightNumber = self.flightNumber
        }
        else if(self.controller == 2)
        {
            let data = "Flight booked on \(self.getDate!)"+"from\(self.getDeparture!)"+"to\(self.getDestination!)"
            values.append(data)
            print(data)
        }
    }
    
    
    
    
}

