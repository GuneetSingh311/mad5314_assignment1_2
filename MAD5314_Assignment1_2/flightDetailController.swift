//
//  flightDetailController.swift
//  MAD5314_Assignment1_2
//
//  Created by Guneet on 2019-03-16.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import WatchConnectivity
class flightDetailController: UIViewController,WCSessionDelegate  {
 
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    // MARK: variables to store values
    var flightNumber:Int?
    var flightDate:String?
    var flightPrice:String?
    var fromPlace:String?
    var toPlace:String?
    
    // MARK: outlets
    
    @IBOutlet weak var Departure: UILabel!
    @IBOutlet weak var Destination: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var Number: UILabel!
    @IBOutlet weak var confirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.confirm.layer.cornerRadius = self.confirm.frame.height/2
        print(flightNumber!)
        self.Departure.text = self.fromPlace!
        self.Destination.text = self.toPlace!
        self.Price.text =   "Price: $\(self.flightPrice!)"
        self.Date.text = "Date: \(self.flightDate!)"
        self.Number.text  = "FlightNumber: \(String(describing: self.flightNumber!))"
        // MARK: Watch connectivity check if the session is supported
        
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
}
    
   @IBAction func confirmAction(_ sender: Any) {
        
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary

            let flightNumber = ["flightNumber": self.flightNumber!]
            //MARK: send flightNumber to watch
            WCSession.default.sendMessage(flightNumber, replyHandler: nil)
            let flightPrice = ["flightPrice":self.flightPrice!]
            //MARK: send flightPrice to watch
            WCSession.default.sendMessage(flightPrice, replyHandler:nil)
            let destination = ["flightDestination": self.fromPlace!]
            //MARK: send destination to watch
            WCSession.default.sendMessage(destination, replyHandler: nil)
            let departure  = ["flightDeparture":self.toPlace!]
            //MARK: send departure to watch
            WCSession.default.sendMessage(departure, replyHandler:nil)
            let date = ["flightDate": self.flightDate!]
            // MARK: send date to watch
            WCSession.default.sendMessage(date, replyHandler: nil)
}
}
    
 

}
